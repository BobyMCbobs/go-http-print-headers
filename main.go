package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"

	"gitlab.com/bobymcbobs/go-http-print-headers/pkg/common"
)

func marshalAsJSON(input interface{}) string {
	outputBytes, _ := json.Marshal(input)
	return string(outputBytes)
}

func logging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(marshalAsJSON(r.Header))
		next.ServeHTTP(w, r)
	})
}

func root(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("Headers logged\n\nHere are yours\n\n%v", marshalAsJSON(r.Header))))
}

func main() {
	log.Printf("launching go-http-print-headers (%v, %v, %v, %v)\n", common.AppBuildVersion, common.AppBuildHash, common.AppBuildDate, common.AppBuildMode)
	port := func() string {
		appPort := os.Getenv("APP_PORT")
		if appPort != "" {
			return appPort
		}
		return ":8085"
	}()
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", root)
	router.Use(logging)
	srv := &http.Server{
		Handler:      router,
		Addr:         port,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Println("HTTP listening on", port)
	log.Fatal(srv.ListenAndServe())
}
